package br.com.controller;

import java.util.LinkedHashSet;



import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Modelo para tag action do xml
 * @author tramalho
 *
 */
@XStreamAlias("action")
public class ActionModel {
	
	@XStreamAsAttribute
	private String type = "";
	
	@XStreamAsAttribute
	private String field = "";

	public ActionModel(String type, String field) {
		super();
		this.type = type;
		this.field = field;
	}

}
