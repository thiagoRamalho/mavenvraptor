package br.com.controller;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("charts")
public class ContainerChart {

	@XStreamImplicit
	private List<PieChart> charts;

	public ContainerChart(List<PieChart> charts) {
		super();
		this.charts = charts;
	}
}
