package br.com.controller;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("documents")
public class ContainerDocument {

	@XStreamImplicit
	private List<DocumentModel> docs;

	public ContainerDocument(List<DocumentModel> docs) {
		super();
		this.docs = docs;
	}
}
