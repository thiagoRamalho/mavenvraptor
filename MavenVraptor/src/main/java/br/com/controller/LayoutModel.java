package br.com.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * Classe que representa um layout de formulario presente no xml
 * 
 * @author tramalho
 *
 */
@XStreamAlias("layout")
public class LayoutModel implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long idDoc;
	
	@XStreamAsAttribute
	private String name;
	
	@XStreamAsAttribute
	private String description;
	
	@XStreamAlias("fields")
	private List<FieldModel> fields = new ArrayList<FieldModel>();
	
	@XStreamAlias("action")
	private ActionModel actionModel;
	
	//xstream
	protected LayoutModel(){}
	
	public LayoutModel(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public ActionModel getActionModel() {
		return actionModel;
	}
	
	public List<FieldModel> getFields() {
		return fields;
	}

	public void addField(FieldModel fieldModel){
		this.fields.add(fieldModel);
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setIdDoc(Long idDoc) {
		this.idDoc = idDoc;
	}
	
	public Long getIdDoc() {
		return idDoc;
	}


	@Override
	public String toString() {
		return "LayoutModel [id=" + id + ", idDoc=" + idDoc + ", name=" + name
				+ ", description=" + description + ", fields=" + fields
				+ ", actionModel=" + actionModel + "]";
	}

	public void setFieldModels(List<FieldModel> fieldModels) {
		this.fields = fieldModels;
	}
}
