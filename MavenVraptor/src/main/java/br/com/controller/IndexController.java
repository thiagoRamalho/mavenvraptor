package br.com.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@Resource
public class IndexController {

	private Result result;

	public IndexController(Result result) {
		super();
		this.result = result;
	}


	@Get("/")
	public void index(){
		this.result.forwardTo(this).home();
	}


	@Get("/home")
	public void home(){
	}

	@Path(value="/document.xml", priority=1)
	public void loadXml() {
		result.use(Results.xml()).from(getData()).recursive().serialize();
	}

	private ContainerChart getPieChart(){
		
		List<PieChart> charts = new ArrayList<PieChart>();
		
		for(int i = 0; i < 3; i++){

			PieChart chart = new PieChart("caption");
			chart.add(new PieChartSet("label", "name", "value"));

			charts.add(chart);
		}

		return new ContainerChart(charts);
	}

	private ContainerDocument getData(){

		List<DocumentModel> docs = new ArrayList<DocumentModel>();

		for(int i = 0 ; i < 3; i++){

			FieldModel fieldModel = new FieldModel("EditText", "EditText"+i);
			fieldModel.setValues(Collections.singletonList(new FieldValue(null, null, "value "+i)));

			LayoutModel layoutModel = new LayoutModel("layout "+i, "description "+i);
			layoutModel.setFieldModels(Collections.singletonList(fieldModel));

			DocumentModel documentModel = new DocumentModel(new Long(i), i, "name "+i);
			documentModel.addLayout(layoutModel);

			docs.add(documentModel);
		}

		return new ContainerDocument(docs);
	}

}
