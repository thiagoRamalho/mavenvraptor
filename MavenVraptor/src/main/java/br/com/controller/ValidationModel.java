package br.com.controller;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("validation")
public class ValidationModel {

	@XStreamAsAttribute
	private String type;

	@XStreamAsAttribute
	private String value;

	@XStreamAsAttribute
	private String message;

	public String getMessage() {
		return message;
	}
	
	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "ValidationModel [type=" + type + ", value=" + value
				+ ", message=" + message + "]";
	}
}
