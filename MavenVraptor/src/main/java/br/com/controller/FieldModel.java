package br.com.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Classe que representa um campo no mapeamento xml
 * 
 * @author tramalho
 *
 */
@XStreamAlias("field")
public class FieldModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long idLayout;
	private Long idDoc;
	
	@XStreamAsAttribute
	private String type;
	
	@XStreamAsAttribute
	private String name;

	@XStreamAsAttribute
	private String inputType;
	
	
	@XStreamImplicit(itemFieldName = "value")
	private List<FieldValue> values = new ArrayList<FieldValue>();	
	
	@XStreamImplicit(itemFieldName = "validation")
	private List<ValidationModel> validations = new ArrayList<ValidationModel>();

	//sxtream
	protected FieldModel(){}

	public FieldModel(String type, String name) {
		super();
		this.type = type;
		this.name = name;
	}
	
	public FieldModel(Long id, Long idLayout, Long idDoc, String type,
			String name, String inputType) {
		super();
		this.id = id;
		this.idLayout = idLayout;
		this.idDoc = idDoc;
		this.type = type;
		this.name = name;
		this.inputType = inputType;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}

	public void setIdDoc(Long idDoc) {
		this.idDoc = idDoc;
	}
	public Long getIdDoc() {
		return idDoc;
	}
	public void setIdLayout(Long idLayout) {
		this.idLayout = idLayout;
	}
	public Long getIdLayout() {
		return idLayout;
	}
	public String getInputType() {
		return inputType;
	}
	
	public String getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
	
	public List<FieldValue> getValues() {
		return values != null ? values : new ArrayList<FieldValue>();
	}
	
	public void setValues(List<FieldValue> values) {
		this.values = values;
	}

	public List<ValidationModel> getValidations() {
		return validations != null ? this.validations : new ArrayList<ValidationModel>();
	}

	@Override
	public String toString() {
		return "FieldModel [type=" + type + ", name=" + name + ", inputType="
				+ inputType + ", values=" + values + ", validations="
				+ validations + "]";
	}
	
	@Override
	public FieldModel clone(){
		return new FieldModel(id, idLayout, idDoc, type, name, inputType);
	}
}
