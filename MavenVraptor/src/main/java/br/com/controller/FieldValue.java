package br.com.controller;


import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@XStreamAlias("value")
public class FieldValue implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long idField;
	private String value;
	
	//xstream
	protected FieldValue() {}
	
	public FieldValue(Long id, Long idField, String value) {
		super();
		this.id = id;
		this.idField = idField;
		this.value = value;
	}
	
	
	public Long getId() {
		return id;
	}
	
	public Long getIdField() {
		return idField;
	}
	
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "FieldValue [id=" + id + ", idField=" + idField + ", value="
				+ value + "]";
	}
}
