package br.com.controller;

import java.util.Calendar;

public class DocumentFile {

	private Long id;
	private long version;
	private String name;
	private String path;
	private Calendar updateDate;
	
	public DocumentFile(Long id, String name, long version, String path) {
		super();
		this.id = id;
		this.version = version;
		this.name = name;
		this.path = path;
	}
	
	public Long getId() {
		return id;
	}
	public long getVersion() {
		return version;
	}
	public String getName() {
		return name;
	}
	public String getPath() {
		return path;
	}
	
	public Calendar getUpdateDate() {
		return updateDate;
	}
	
	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
