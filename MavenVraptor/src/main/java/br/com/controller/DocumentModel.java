package br.com.controller;


import java.io.Serializable;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Classe que representa o documento xml contendo os layouts dos
 * formularios
 * 
 * @author tramalho
 *
 */
@XStreamAlias("document")
public class DocumentModel implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@XStreamAsAttribute
	private long version;
	
	@XStreamAsAttribute
	private String name;
	
	private Calendar updateDate;

	//xstream
	protected DocumentModel(){}
	
	public DocumentModel(Long id, long version, String name) {
		super();
		this.id = id;
		this.version = version;
		this.name = name;
	}
	
	@XStreamImplicit(keyFieldName="name")
	private LinkedHashMap<String, LayoutModel> layouts = new LinkedHashMap<String, LayoutModel>();
	
	public void setLayouts(LinkedHashMap<String, LayoutModel> layouts) {
		this.layouts = layouts;
	}
	
	public Map<String, LayoutModel> getLayoutsModel() {
		return layouts;
	}
	
	public Long getId() {
		return id;
	}

	public long getVersion() {
		return version;
	}

	public String getName() {
		return name;
	}
	
	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}
	
	public Calendar getUpdateDate() {
		return this.updateDate;
	}
	
	@Override
	public String toString() {
		return "DocumentModel [id=" + id + ", version=" + version + ", name="
				+ name + ", layouts=" + layouts + "]";
	}

	public void clearLayout() {
		this.layouts  = new LinkedHashMap<String, LayoutModel>();
	}

	public LayoutModel getLayoutByName(String name) {
		return this.layouts.get(name);
	}

	public void addLayout(LayoutModel layoutModel) {
		this.layouts.put(layoutModel.getName(), layoutModel);
	}

	public void setId(Long id) {
		this.id = id;
	}
}
