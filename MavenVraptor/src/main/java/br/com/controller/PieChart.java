package br.com.controller;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "graph")
	public class PieChart{
		
		@XStreamAsAttribute
		private String caption;
		
		@XStreamImplicit
		private List<PieChartSet> labels = new ArrayList<PieChartSet>();

		public PieChart(String caption) {
			this.caption = caption;		
		}

		public void add(PieChartSet pieChartSet) {
			labels.add(pieChartSet);
		}

		public String getCaption() {
			return caption;
		}

		public int getX() {
			// TODO Auto-generated method stub
			return 0;
		}

		public int getY() {
			// TODO Auto-generated method stub
			return 0;
		}
	}
